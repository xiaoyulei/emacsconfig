;;; -*- no-byte-compile: t -*-
(define-package "company-go" "20170825.943" "company-mode backend for Go (using gocode)" '((company "0.8.0") (go-mode "1.0.0")) :commit "1a78dd6c36d7f37cbb60073523c2e9ca3f6519fa" :keywords '("languages"))
